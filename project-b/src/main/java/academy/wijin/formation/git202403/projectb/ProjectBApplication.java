package academy.wijin.formation.git202403.projectb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectBApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectBApplication.class, args);
	}

}
